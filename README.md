## Super's Custom Script Modules for RetroPie

## Installation
Run these commands, in order:
```bash
cd ~/RetroPie-Setup/
git clone https://gitlab.com/SuperFromND/retropie-custom-scripts.git
sudo cp -R retropie-custom-scripts/scriptmodules/* ~/RetroPie-Setup/scriptmodules
rm -rf ~/RetroPie-Setup/retropie-custom-scripts
```
A quick and easy way to do this is to run [PuTTY](https://www.putty.org/), copy the commands, and right click to paste the entire set at once.
## Usage

Once installed, these scripts act just like any other script module.  They can be launched from the RetroPie Setup utility.

## Script List

 - lr-np2kai - PC-9801 libretro core.  Has been accepted into the official RetroPie-Setup repo.  Requires the following BIOS files:

| File | md5sum | CRC32 |
| -- | -- | -- |
| 2608_BD.WAV | 29AAD51CD243C8E449D311D14613F0B1 | FCB60C01 |
| 2608_HH.WAV | 59A009EE444318BD57D99A19068731E4 | 7D6D9C4E |
| 2608_RIM.WAV | 943290D1C5C6AE6295BD02BE4411C7C0 | 8518A388 |
| 2608_SD.WAV | C99156118789B6CBA662C864EBADC62E | C977FDB8 |
| 2608_TOM.WAV | C321A6835B26AD125B2EB78BE56394A4 | 5E8AB475 |
| 2608_TOP.WAV | 9E73FF2345236EBE72F7A937E477F0BD | CEFA9F76 |
| BIOS.ROM | E246140DEC5124C5E404869A84CAEFCE | 76AFFD90 |
| FONT.ROM | 2AF6179D7DE4893EA0B705C00E9A98D6 | CD6DFABE |
| SOUND.ROM | CAF90F22197AED6F14C471C21E64658D | A21EF796 |
| ITF.ROM | E9FC3890963B12CF15D0A2EEA5815B72 | 273E9E88 |
 - lr-simcoupe - MGT Sam Coupe libretro core.
 - lr-eightyone/81 - ZX80/ZX81 libretro core.  Has been accepted into the official RetroPie-Setup repo.
 - gnash - Open-source Adoble Flash SWF file player.

## Disclaimer
I did not write the actual emulators that these scripts install;  I merely wrote the scripts to install them.  If you don't want a certain script containing your software distributed, message me and I'll remove it.
